Stations_ID;Stationsname;Geo. Laenge [Grad];Geo. Breite [Grad];Stationshoehe [m];Geberhoehe ueber Grund [m];Von_Datum;Bis_Datum;Geraetetyp Name;Messverfahren;eor;
2629;Kleve;6.12;51.78;46;1;19471111;19471116;Hellmann;Niederschlagsmenge, konv.;eor;
2629;Kleve;6.15;51.8;13;1;19471117;19520831;Hellmann;Niederschlagsmenge, konv.;eor;
2629;Kleve;6.13;51.78;44;1;19520901;19540731;Hellmann;Niederschlagsmenge, konv.;eor;
2629;Kleve;6.14;51.79;36;1;19540801;19640113;Hellmann;Niederschlagsmenge, konv.;eor;
2629;Kleve;6.13;51.79;22;1;19640114;19720628;Hellmann;Niederschlagsmenge, konv.;eor;
2629;Kleve;6.15;51.77;45;1;19720629;19910630;Hellmann;Niederschlagsmenge, konv.;eor;
2629;Kleve;6.1;51.76;46;1;20040701;20181022;PLUVIO;Niederschlagsmenge, elektr.;eor;
2629;Kleve;6.1;51.76;46;1;20181023;20190304;rain[e]H3, Wägetechnologie;Niederschlagsmenge, elektr.;eor;
generiert: 06.03.2019 --  Deutscher Wetterdienst  --
